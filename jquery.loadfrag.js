(function( $ ) {
    $.fn.loadfrag = function(options) {
        // This plugin loads HTML from a URL and injects it into a given place in the DOM. 
        // It may also optionally inject sections of the remote document when given a selector.
        // `options` is at least the URL of the HTML to load. A selector for the section of the 
        // remote document can be specified when separated from the URL by a space.
        var dest = this; // The place in the DOM this plugin is relative to.
        var ops = options.split(" ");
        var doc = ops[0]; // The remote HTML document to fetch.
        var selector = ops[1]; // The selector for the desired section(s) of the remote document.
        $.get(
	    doc, function(data){
            if (selector){
                $(dest).html($(data).filter(selector));
            } else {
                $(dest).html($(data));
            }
        }, 'html');
        return;
    };
})( jQuery );
